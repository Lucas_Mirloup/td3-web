<?php
include_once 'db_connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<body>
<?php
if (auth() or (isset($_POST['username']) and isset($_POST['password']) and auth($_POST['username'],$_POST['password']))){ ?>
    <?php echo '<p>Logged in as <a href="user.php?name=' . $_SESSION['username'] .'">' . $_SESSION['username'] . '</a></p>' ?>
    <form id="login" action="logout.php">
        <input type="submit" value="Logout" />
    </form>
<?php }
else { ?>
    <form id="login" action="login.php" method="POST">
        <label for="username">Username :</label>
        <input id="username" name="username" type="text" size="10">
        <label for="password">Password :</label>
        <input id="password" name="password" type="password" size="10">
        <input type="submit" value="Login">
    </form>
    <form id="register" action="register.php" method="POST">
        <input type="submit" value="Register">
    </form>
<?php } ?>
</body>
</html>
