<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>GameNet - Games </title>
    <link rel="stylesheet" href="./css/images.css">
</head>
<body>

<?php
include "template_header.php";
include_once 'db_connect.php';

$g = $GLOBALS["db"]->query("SELECT idGame,title,description FROM GAMES WHERE title='" . $_GET['title'] . "'")->fetch();
?>

<form class="" action="add.php" method="get">
    <input hidden type="text" name="idGame" value="<?php echo $g['idGame']?>">
    <input value="Add this game to my library" type="submit">
</form>
<h1> <?php echo $g['title'] ?> </h1>
<div>
    <h2>Description of <?php echo $g['title'] ?></h2>
    <p><?php echo $g['description'] ?></p>

</div>
<div>
    <h2>Images of <?php echo $g['title'] ?></h2>
    <?php
    echo '<img src="./img/'.$g['idGame'].'.png" alt="Error while loading image."';
    ?>
</div>
<div>
    <h2>Players of <?php echo $g['title'] ?></h2>
    <ul>
        <?php
        $users = $GLOBALS["db"]->query("SELECT username FROM OWNS NATURAL JOIN GAMES NATURAL JOIN USERS where idGame='" . $g['idGame'] . "'")->fetchAll();
        echo "<ul>";
        if (count($users) == 0){
            echo "<li>No players for ", $g['title'], " for the moment.</li>";
        }
        else foreach($users as $user){
            echo "<li><a href='user.php?name=" . $user['username'] . "'>" . $user['username'] . "</a></li>";
        }
        echo "</ul>";
        ?>
    </ul>
</div>
</body>
</html>
