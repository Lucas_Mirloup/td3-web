<?php
include_once 'db_connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<body>
<?php
if (auth() or (isset($_POST['username']) and isset($_POST['password']) and auth($_POST['username'],$_POST['password']))){
    header('Location: user.php?name=' . $_SESSION['username']);
}
else { include 'template_header.php' ?>
    <form id="login" action="login.php" method="POST">
        <p>
            <label for="username">Username :</label>
            <input id="username" name="username" type="text">
        </p>
        <p>
            <label for="password">Password :</label>
            <input id="password" name="password" type="password">
        </p>
        <p>
            <input type="submit" value="Valider">
        </p>
    </form>
    <form id="register" action="register.php" method="POST">
        <p>
            <input type="submit" value="Register">
        </p>
    </form>
<?php } include 'template_footer.php' ?>
</body>
</html>
