<?php include_once 'db_connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
<?php
include 'template_header.php';
if (auth()){ ?>
    <p>Logged in as <?php echo $_SESSION['username']?></p>
    <form action="logout.php">
        <input type="submit" value="Logout" />
    </form>
<?php }
else if (isset($_POST['username']) and isset($_POST['password']) and isset($_POST['firstname']) and isset($_POST['lastname'])
    and !empty($_POST['username']) and !empty($_POST['password']) and !empty($_POST['firstname']) and !empty($_POST['lastname']))
{
    try {
        insert_user($_POST['username'], $_POST['password'], $_POST['firstname'], $_POST['lastname']);
    }
    catch (PDOException $e){
        $_SESSION['username'] = $_POST['username'];
    }
    finally {
        auth($_POST['username'], $_POST['password']);
        header('Location: user.php?name=' . $_POST['username']);
    }
    //$user = $GLOBALS["db"]->query("SELECT username FROM USERS where username = '" . $_POST['username'] . "'")->fetch();
    //echo $user['username'];
}
else { ?>
    <form action="register.php" method="POST">
        <p>
            <label for="firstname">First Name :</label>
            <input id="firstname" name="firstname" type="text">
        </p>
        <p>
            <label for="lastname">Last Name :</label>
            <input id="lastname" name="lastname" type="text">
        </p>
        <p>
            <label for="username">Username :</label>
            <input id="username" name="username" type="text">
        </p>
        <p>
            <label for="password">Password :</label>
            <input id="password" name="password" type="password">
        </p>
        <p>
            <input type="submit" value="Valider">
        </p>
    </form>
<?php } include 'template_footer.php' ?>
</body>
</html>
