<?php
function insert_user($username,$password,$firstname,$lastname){
    $insert_user="INSERT INTO USERS (username, password, firstname, lastname) VALUES (:username, :password, :firstname, :lastname)";
    $stmt = $GLOBALS["db"]->prepare($insert_user);
    $stmt->execute([':username' => $username, ':password' => $password,':firstname' => $firstname,':lastname' => $lastname]);
}

function insert_game($title,$description){
    $insert_game="INSERT INTO GAMES (title, description) VALUES (:title, :description)";
    $stmt = $GLOBALS["db"]->prepare($insert_game);
    $stmt->execute([':title' => $title,':description' => $description]);
}

function add_game($idUser,$idGame){
    $add_game="INSERT INTO OWNS (idUser, idGame) VALUES (:idUser, :idGame)";
    $stmt = $GLOBALS["db"]->prepare($add_game);
    $stmt->execute([':idUser' => $idUser,':idGame' => $idGame]);
}