<?php
include_once 'db_connect.php';
if (auth() and isset($_GET['idGame'])){
    try{
        add_game($_SESSION['idUser'],$_GET['idGame']);
    }
    catch (PDOException $e){}
    finally {
        header('Location: user.php?name=' . $_SESSION['username']);
    }
}
else{
    header('Location: user.php?name=' . $_SESSION['username']);
}