<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>About - GameNet</title>
  </head>
  <body>
    <?php include "template_header.php" ?>
    <h1>About</h1>
    <div>
      <p>
        This website has been developped in for a Web server exercise at the Orleans' IUT, at the Computer Science department.<br>
        The owners of the right on this website are the students who had developped it.<br>
        No copyright nor commercial use. Free use only.<br>
      </p>
    </div>
    <?php include "template_footer.php" ?>
  </body>
</html>

<!-- Site développé dans le cadre d'un exercice de Web Serveur à l'IUT d'Orléans, département informatique.</br> -->
<!-- Tout droits réservés aux étudiants qui l'ont développé.</br> -->
<!-- Aucun droit commercial n'est associé à l'utilisation du site.</br> -->
