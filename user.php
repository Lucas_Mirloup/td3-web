<?php
include_once 'db_connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GameNet - Profile</title>
</head>
<body>
<?php include "template_header.php";
if (isset($_GET['name']) and !empty($_GET['name'])) {
    if (auth()) { ?>
        <h1>Your account - GameNet</h1>
        <h2>Your games</h2>
        <p>
            <?php
            $games = $GLOBALS["db"]->query("SELECT title FROM GAMES NATURAL JOIN OWNS NATURAL JOIN USERS WHERE username='" . $_GET['name'] . "'")->fetchAll();
            echo "<ul>";
            if (count($games) == 0){
                echo "<li>Your GameList is empty ! You can add some games to your account <a href='game.php'>here</a>.</li>";
            }
            else foreach($games as $game){
                echo "<li><a href='info.php?title=" . $game['title'] . "'>" . $game['title'] . "</a></li>";
            }
            echo "</ul>";
            ?>
        </p>
    <?php }
    else {
        echo $_GET['name'] . "'s account - GameNet";
        $games = $GLOBALS["db"]->query("SELECT title FROM GAMES NATURAL JOIN OWNS NATURAL JOIN USERS WHERE username='" . $_GET['name'] . "'")->fetchAll();
        echo "<ul>";
        if (count($games) == 0){
            echo "<li>Empty GameList.</li>";
        }
        else foreach($games as $game){
            echo "<li>" . $game['title'] . "</li>";
        }
        echo "</ul>";
    }
} else{
    $users = $GLOBALS["db"]->query('SELECT username FROM USERS ORDER BY username ASC');
    echo "<h2>Users :</h2><ul>";
    foreach($users as $user){
        echo "<li><a href='user.php?name=" . $user['username'] . "'>" . $user['username'] . "</a></li>";
    }
    echo "</ul>";
} include "template_footer.php" ?>
</body>
</html>
