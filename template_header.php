<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="./css/header.css">
<body>
<header>
    <h2><a href="index.php">GameNet</a></h2>
    <nav>
        <ul>
            <li> <a href="game.php">Games</a> </li>
            <li><a href="user.php">Users</a></li>
            <li> <a href="about.php">About</a> </li>
        </ul>
    </nav>
    <?php include 'search_header.php';
    include 'login_header.php' ?>
</header>
</body>
</html>
