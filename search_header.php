<!DOCTYPE html>
<html lang="en">
<body>
    <form id="search-bar" action="search.php" method="GET">
        <select name="t">
            <option selected value="a">All</option>
            <option value="g">Games</option>
            <option value="u">Users</option>
        </select>
        <input id="q" type="text" name="q" placeholder="Ex: Minecraft" size="10">
        <input type="submit" value="Search">
    </form>
</body>