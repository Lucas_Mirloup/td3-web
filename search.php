<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="./css/style.css">
<body>
<?php
include_once 'db_connect.php';
include_once 'template_header.php';

if (isset($_GET['t']) and isset($_GET['q'])) {
    if ($_GET['t'] == 'g' or $_GET['t'] == 'a') {
        $games = $GLOBALS["db"]->query("SELECT title FROM GAMES WHERE title LIKE '%" . $_GET['q'] . "%'")->fetchAll();
        echo "<h2>Games</h2>";
        if (count($games) == 0) {
            echo "There is no game matching your search.";
        } else {
          ?> <form action="info.php" method="get"><?php
            foreach ($games as $game) {
                echo "<input name=\"title\" value=\"" . $game['title'] . "\" type=\"submit\">";
            }
          ?> </form> <?php
        }
    }

    if ($_GET['t'] == 'u' or $_GET['t'] == 'a') {
        $users = $GLOBALS["db"]->query("SELECT username FROM USERS WHERE username LIKE '%" . $_GET['q'] . "%'")->fetchAll();
        echo "<h2>Users</h2>";
        if (count($users) == 0) {
            echo "There is no user matching your search.";
        } else {
            echo "<ul>";
            foreach ($users as $user) {
                echo "<li>" . $user["username"] . "</li>";
            }
            echo "</ul>";
        }
    }
} else {
    echo "<h1>An error occurred, please search again.</h1>";
} ?>
</body>
